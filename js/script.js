'use strict';

class Card {
    constructor(name, url, value) {
        this.name = name;
        this.url = url;
        this.value = value;
    }
}

var deck = new Array();
var sumDealer = 0;
var sumPlayer = 0;

const initDeck = () => {
    for(let i=2; i<=10 ;i++) {
      const cardC = new Card(`${i}C`,`./cards/${i}C.jpg`, i);
      const cardD = new Card(`${i}D`,`./cards/${i}D.jpg`, i);
      const cardH = new Card(`${i}H`,`./cards/${i}H.jpg`, i);
      const cardS = new Card(`${i}S`,`./cards/${i}S.jpg`, i);
      deck.push(cardC);
      deck.push(cardD);
      deck.push(cardH);
      deck.push(cardS);
    }
    ['A','J','Q','K'].forEach(i => {
        deck.push(new Card(`${i}C`,`./cards/${i}C.jpg`, 10));
        deck.push(new Card(`${i}D`,`./cards/${i}D.jpg`, 10));
        deck.push(new Card(`${i}H`,`./cards/${i}H.jpg`, 10));
        deck.push(new Card(`${i}S`,`./cards/${i}S.jpg`, 10));
    });
}

initDeck();

const getRandom = () => {
    return Math.floor(Math.random()*(deck.length-1));
}

const showWinner = (result) => {
    // todo: show a bootstrap modal for result
   const message = 'You '+ result + '\n'+ 'Dealer score: ' + sumDealer + '\n' + 'Your score: '+ sumPlayer;
   confirm(message);
}

const deal = () => {
    // todo: disable stick and hit button before click deal button
    document.getElementById('deal').disabled = true;
    let dealersDesk = document.getElementById('dealers');
    let playersDesk = document.getElementById('players');
    let indexC = getRandom();

    let dealerCard = document.createElement('img');
    dealerCard.src = deck[indexC].url;
    sumDealer += deck[indexC].value;
    deck.splice(indexC, 1);
    dealerCard.className = 'card';
    dealersDesk.appendChild(dealerCard);

    indexC = getRandom();
    let dealerCardReverse = document.createElement('img');
    dealerCardReverse.src = deck[indexC].url;
    sumDealer += deck[indexC].value;
    dealerCardReverse.className = 'card reverse';
    deck.splice(indexC, 1);
    dealerCardReverse.style.visibility='hidden'; 
    dealersDesk.appendChild(dealerCardReverse);

    let playersCard1 = document.createElement('img');
    indexC = getRandom();
    playersCard1.src = deck[indexC].url;
    if (deck[indexC].name.includes('A') && sumPlayer > 10) {
        sumPlayer += 1;
    } else {
        sumPlayer += deck[indexC].value;
    }
    deck.splice(indexC, 1);
    playersCard1.className = 'card';
    playersDesk.appendChild(playersCard1);
    
    let playersCard2 = document.createElement('img');
    indexC = getRandom();
    playersCard2.src = deck[indexC].url;
    if (deck[indexC].name.includes('A') && sumPlayer > 10) {
        sumPlayer += 1;
    } else {
        sumPlayer += deck[indexC].value;
    }
    deck.splice(indexC, 1);
    playersCard2.className = 'card';
    playersDesk.appendChild(playersCard2);
    let score = document.getElementById('score');
    score.style.color = 'beige';
    score.style.fontSize ='3em';
    score.innerHTML = `Your score is: ${sumPlayer}`;
}

const hit = () => {
    let dealersDesk = document.getElementById('dealers');
    let playersDesk = document.getElementById('players');
    let playersCard1 = document.createElement('img');
    let indexC = getRandom();
    playersCard1.src = deck[indexC].url;
    if (deck[indexC].name.includes('A') && sumPlayer > 10) {
        sumPlayer += 1;
    } else {
        sumPlayer += deck[indexC].value;
    }
    deck.splice(indexC, 1);
    playersCard1.className = 'card';
    playersDesk.appendChild(playersCard1);
    let score = document.getElementById('score');
    score.style.color = 'beige';
    score.style.fontSize ='3em';
    score.innerHTML = `Your score is: ${sumPlayer}`;

    let dealerCard = document.createElement('img');
    dealerCard.src = deck[indexC].url;
    sumDealer += deck[indexC].value;
    deck.splice(indexC, 1);
    dealerCard.className = 'card';
    dealerCard.style.visibility = 'hidden';
    // todo: show dealers card at the end
    if (sumPlayer > 21) {
        showWinner('lose!');
    }
    
    if(sumDealer > 21) {
        showWinner('win!');
    }
}

const stick = () => {
    if (sumPlayer <= 21 && sumDealer <= sumPlayer) {
        showWinner('win!');
    } else {
        showWinner('lose!');
    }
}
const reset = () => {
    sumDealer = 0;
    sumPlayer = 0;
    initDeck();
    let dealersDesk = document.getElementById('dealers');
    let playersDesk = document.getElementById('players');
    let score = document.getElementById('score');
    score.innerHTML ='';
    while (dealersDesk.hasChildNodes()) {
        dealersDesk.removeChild(dealersDesk.lastChild);
    }

    while (playersDesk.hasChildNodes()) {
        playersDesk.removeChild(playersDesk.lastChild);
    }
    document.getElementById('deal').disabled = false;
}