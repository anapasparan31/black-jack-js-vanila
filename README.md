# BlackJack - Ana Maria Pasparan

Blackjack, also known as twenty-one, is a comparing card game between usually several players and a dealer, where each player in turn competes against the dealer, but players do not play against each other. It is played with one or more decks of 52 cards, and is the most widely played casino banking game in the world. The objective of the game is to beat the dealer in one of the following ways:

 Get 21 points on the player's first two cards (called a "blackjack" or "natural"), without a dealer blackjack;
 Reach a final score higher than the dealer without exceeding 21; or
 Let the dealer draw additional cards until their hand exceeds 21.
(Wikipedia: https://en.wikipedia.org/wiki/Blackjack)
This application was developed with ES6, Bootstrap4, HTML5, CSS3.

## Installation

No need.

## Usage

Open the index.html in Chrome
The application layout depends on the browser.

# Browser compatibility

The application is not compatible with IE